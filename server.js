const express=require('express'),passport=require('passport'),
Strategy=require('passport-facebook').Strategy;
//creates strategy
passport.use(new Strategy(
    {clientID:'',clientSecret:'',
callbackURL:'http://localhost:3000/login/facebook/return'},
(accessToken,refreshToken,profile,cb)=>cb(null,profile)));
passport.serializeUser((user,cb)=>{cb(null,user);});
passport.deserializeUser((obj,cb)=>{cb(null,obj);});
const app=express();app.set('view engine','ejs');app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());app.use(require('body-parser').urlencoded({extended:true}));
app.use(require('express-session')({secret:'LCO App',resave:true,saveUninitialized:true}));
/* 
@route - GET / home
@desc - A route to home
@access - PUBLIC
*/
app.get("/",(req,res)=>{res.render('home',{user:req.user});});
/* 
@route - GET / login
@desc - A route to login
@access - PUBLIC
*/
app.get('/login',(req,res)=>{res.render('login');});
/* 
@route - GET / login/facebook
@desc - A route to facebookauth
@access - PUBLIC
*/
app.get('/login/facebook',passport.authenticate('facebook'));
/* 
@route - GET / login/facebook/callback
@desc - A route to facebookauth
@access - PUBLIC
*/
app.get('/login/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });

/* 
@route - GET / profile
@desc - A route to profile of user
@access - PRIVATE
*/
app.get('/profile',require('connect-ensure-login').ensureLoggedIn(),(req,res)=>
res.render('profile',{user:req.user}));
app.listen(3000,()=>console.log("Server is running at port: 3000 ...."))